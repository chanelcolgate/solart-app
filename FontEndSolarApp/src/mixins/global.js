import axios from 'axios';

export default {
  methods: {
    baseurl(){
      return "http://103.28.32.80:8080/api/";
    },
    baseUrlServer() {
      // return "http://103.28.32.80:8091/";
      return "http://192.168.1.7:8081/"
    },
    async request(link, data = {}, method='post') {
      if(method === 'get'){
        data = {params: data}
      }
      let response = await axios[method.toLowerCase()](link, data);
      return response.data;
    },
    requestdata(link, data) {
      return axios.post(link, data, {headers: {'Content-Type': 'multipart/form-data'}})
          .then((response) => {
            return response.data;
          });
    },
    generate_code(){
      let u = Date.now().toString(16) + Math.random().toString(16) + '0'.repeat(16);
      return [u.substr(0,8), u.substr(8,4), '4000-8' + u.substr(13,3), u.substr(16,12)].join('-');
    },
    // check_allow_character(txt){
    //   allow_chars = ['a','b','c','d','e','f','g','h','i']
    // },
    filter_all_column(arr, txt){
      if(txt === '' || typeof txt === 'undefined'){
        return arr;
      }else{
        let val = [];
        let to_return = [...arr].filter(e=>{
          val = Object.values(e);
          let ret = false;
          for (let i = 0; i < val.length; i++) {
            if(typeof val[i] === 'string'){
              if(this.bodautiengviet(val[i].toLowerCase()).includes(this.bodautiengviet(txt.toString().toLowerCase()))){
                ret = true
              }
            }
          }
          return ret;
        });
        return to_return;
      }
    },
    check_is_login() {
      if (localStorage.getItem("username") === null) {
        this.$router.push('/login');
      }
    },
    goToPage(newlink) {
      // window.location.href = '/' + newlink;
      this.$router.push(newlink)
    },
    formatNumberToFixed(num, pFixed) {
      let mvar = parseFloat(num);
      mvar = mvar.toFixed(pFixed);
      return mvar.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    },
    numberWithFormat(x, separator) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, separator);
      var decimal = ".";
      if (separator == '.') {
        decimal = ",";
      }
      return parts.join(decimal);
    },
    // bodautiengviet(str) {
    //   str = str.toString().toLowerCase();
    //   str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    //   str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    //   str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    //   str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    //   str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    //   str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    //   str = str.replace(/đ/g, "d");
    //   str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    //   str = str.replace(/ + /g, " ");
    //   str = str.trim();
    //   return str;
    // },
    updateArrayObject(pArray, pField, pObjectUpdate) {
      try {
        let mindex = -1;
        pArray.forEach((el, index) => {
          if (el[pField] === pObjectUpdate[pField]) mindex = index;
        });
        pArray[mindex] = pObjectUpdate;
        return pArray;
      } catch (e) {
        return pArray;
      }
    },
    addArrayObject(pArray, pField, pObjectUpdate) {
      try {
        let mindex = -1;
        pArray.forEach((el, index) => {
          if (el[pField] === pObjectUpdate[pField])
            mindex = index;
        });
        if (mindex < 0) {
          pArray.push(pObjectUpdate);
        } else {
          pArray[mindex] = pObjectUpdate;
        }
        return pArray;
      } catch (e) {
        return pArray;
      }
    },
  }
}
