module.exports = {
	runtimeCompiler: true,
	assetsDir: 'static',
	publicPath: '/',
	devServer: {
		disableHostCheck: true,
		port: 8080,
		public: '0.0.0.0:8080'
	}
}