const state = {
	loading: false,
};

const mutations = {
	UPDATE_LOADING(state, payload) {
		state.loading = payload;
	},
};

const actions = {
	setLoading({commit}, value) {
		commit('UPDATE_LOADING', value);
	},
};

const getters = {
	loading: state => state.loading,
};

const controllerModule = {
	state,
	mutations,
	actions,
	getters
};

export default controllerModule;