import Vue from 'vue';
import Vuex from 'vuex';
import controllers from '@/store/modules/controllers'

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		controllers,
	},
});