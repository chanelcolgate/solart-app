import Vue from 'vue'
import App from './App.vue'
import router from '@/router/'
import store from '@/store/'
import axios from 'axios'
import VueAxios from 'vue-axios'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import VueSession from 'vue-session'
import global from '@/mixins/global'
import '@/lib/css';
import '@/lib/script';

Vue.config.productionTip = false

Vue.use(VueAxios, axios);
Vue.use(VueMomentJS, moment);
Vue.use(VueSession);
Vue.mixin(global);

new Vue({
  router,
  store,
  components: {
    App,
  },
  render: h => h(App),
}).$mount('#app')
