import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

function load(component) {
	return () => import(`@/components/${component}`)
}

export default new Router({
	mode: 'history',
	routes: [
		{path: '/login', component: load('Login')},
		{
			path: '/', component: load('partial/Header')
		},
	]
})